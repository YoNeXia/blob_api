
# Blob's API  
#### Blob's API is a Node.js API to make request in a mongo database and return responses in JSON format.  
  

## Documentation

This documentation is up to date with version "1.1.0".

## About  
  
The API was made with Node.js, MongoDB and Docker. We use a nodejs image from docker hub in a container named **blob_api**. When you run the container, it is going open port 8080 for http, on your system.  
  
## Getting started  
  
**How to initialize the API :**  
  
To initialize API, follow the instructions in this git repository: [Git](https://git.seppa.io/infra/blobAPI). This one permit to deploy API from any server. If not and you did not succeed, please contact support.  
  
   
   
 -----------------  
## Problems with the API  
  
**How to restart your API :**  
Run the command: _(blob_api is the name of the service and container)_.
  
	docker restart blob_api
	
Or to get informations from the terminal of your container. 

    docker stop blob_api
    
And after, rebuild your container.

	docker-compose up  
	
Finally, check if your container is running.  

	docker ps -a
	
**Precaution in _config.json_ :**

He is used to be able to connect any database from any server in API. When you create a customer in API, **don't forget to add it in config.json**.

**Problems in localhost :**  

This version is developed to be use on a server. So, on your localhost, you need to modify the connection to the mongo database. _(modify config.json)_ And you need to get certificates, and modify path to open them. Or delete all things concern https connection. _(modifications needed in token_management.js)_  

Find more information about files in documentation about them.  

If you don't want use docker's container, use command in the same directory where you have the _package.json_ file, to launch the script _blob_api_script.sh_:  

	npm start 
	
## Authors  

- **SEPPA** - _Initial Work_ - [Website](https://agence-seppa.com)
- **YoNeXia** - _Initial Work_ - [Git](https://gitlab.com/YoNeXia)

## Code quality

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=YoNeXia_blob_api&metric=alert_status)](https://sonarcloud.io/dashboard?id=YoNeXia_blob_api)
