/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 04/04/19 16:48
 */

// Import mongoose modules
const Mongoose = require('mongoose');

// Setup Landing Page schema
const landing_schema = Mongoose.Schema({
    created: Mongoose.Schema.Types.Date,
    deleted: {type: Boolean, default: false},
    campaign_id: String,
}, {strict: false});

// Export Landing Page model
module.exports.landing_schema = landing_schema;