/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 05/04/19 13:01
 */

// Import modules
const Mongoose = require('mongoose');

// Set up User schema
const user_schema = Mongoose.Schema({
    gender: String,
    name: String,
    surname: String,
    phone: String,
    email: String,
    password: String,
    last_visit: Mongoose.Schema.Types.Date,
    role: String,
    tips: {},
    deleted: {type: Boolean, default: false},
    customer_id: String,
});

// Export User model
module.exports.user_schema = user_schema;