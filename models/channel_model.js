/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 09/04/19 09:53
 */

// Import mongoose modules
const Mongoose = require('mongoose');

// Setup Channel schema
const channel_schema = Mongoose.Schema({
    name: String
});

// Export Channel model
const Channel = module.exports = Mongoose.model('channels', channel_schema);
module.exports.get = (callback, limit) => {
    Channel.find(callback).limit(limit);
};