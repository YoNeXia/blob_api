/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 05/04/19 11:24
 */

// Import mongoose modules
const Mongoose = require('mongoose');

// Setup Company schema
const customer_schema = Mongoose.Schema({
    name: String,
    location: {
        address: String,
        city: String,
        postcode: String,
        country: String,
    },
    logo_url: String,
    created: Mongoose.Schema.Types.Date,
    deleted: {type: Boolean, default: false}
});

// Export Customer model
module.exports.customer_schema = customer_schema;