/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 04/04/19 16:50
 */

// Import mongoose modules
const Mongoose = require('mongoose');

// Setup Campaign schema
const campaign_schema = Mongoose.Schema({
    created: Mongoose.Schema.Types.Date,
    deleted: {type: Boolean, default: false},
}, {strict: false});

// Export Campaign model
module.exports.campaign_schema = campaign_schema;