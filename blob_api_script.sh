#!/bin/bash

# Make an update of all plugins using in Linux
apt update
# Get the modules declared in package.json
npm install
# Install the module bcrypt (because he is different between OS)
npm install bcrypt
# Launch the API
node app.js