/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 09/04/19 14:47
 */

// Import modules
const Database = require('../database_management');
const Response = require('../responses');
const Token = require('../token_management');
const Mongoose = require('mongoose');

Mongoose.Promise = Promise;

// Return Landing Model
getLandingModel = (token) => {
    return Database.connections[Database.getCustomer(token)].landings
};

// Return Campaign Model
getCampaignModel = (token) => {
    return Database.connections[Database.getCustomer(token)].campaigns
};

// Create Landing action
exports.create = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Get Landing Model
    let Landing = getLandingModel(token);
    // Create new landing page, with received parameters
    const new_landing = new Landing(req.body);
    // Set the date when landing was created
    new_landing.created = new Date();
    // Save landing
    new_landing.save((err) => {
        // If get error, return it
        if (err)
            return Response.getResponse(res, 500, 'Internal server error...', err);
        // Save the data in database
        return Response.getResponse(res, 200, 'New landing created!', new_landing);
    });
};

// Update by ID Landing action
exports.update = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Get Landing Model
    let Landing = getLandingModel(token);
    // Find landing you want to change data
    const landing_update = Landing.findOne({_id: req.params.id, deleted: false}, (err, find) => {
        if (!find)
            return Response.getResponse(res, 400, 'None landing found for this ID.', null);
        // Update the data in database
        landing_update.updateOne({$set: req.body}, (err) => {
            // If get error, return it
            if (err)
                return Response.getResponse(res, 500, 'Internal server error...', err);
            // Return Landing updated
            return Response.getResponse(res, 200, 'Landing updated!', null);
        })
    });
};

// Delete by ID Landing action
exports.delete = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Get Landing Model
    let Landing = getLandingModel(token);
    // Find landing you want to change data
    const landing_deleted = Landing.findOne({_id: req.params.id, deleted: false}, (err, find) => {
        if (token.role === 'CUSTOMER')
            return Response.getResponse(res, 401, 'User not authorized', null);
        if (!find)
            return Response.getResponse(res, 400, 'None landing found for this ID.', null);
        // Delete the data in database
        landing_deleted.updateOne({$set: {deleted: true}}, (err) => {
            // If get error, return it
            if (err)
                return Response.getResponse(res, 500, 'Internal server error...', err);
            // Return Landing deleted
            return Response.getResponse(res, 200, 'Landing deleted!', null);
        })
    });
};

// Find by ID User action
exports.find = (req, res) => {
    // Get valid token
    const token = Token.getToken(req);
    // Get Landing Model
    let Landing = getLandingModel(token);
    // Parameters for the query
    const params = {
        deleted: 0,
        created: 0,
        __v: 0
    };
    // Find landing you want to find data
    const landing_find = Landing.find({_id: req.params.id, deleted: false});
    // Found the actual landing
    landing_find.findOne({}, params, (err, find) => {
        // If none landing found
        if (!find)
            return Response.getResponse(res, 400, 'None landing found for this ID.', null);
        // If get error, return it
        if (err)
            return Response.getResponse(res, 500, 'Internal server error...', err);
        // Send the response with landing data
        return Response.getResponse(res, 200, 'Landing details fetched...', find);
    })
};

// Find all landings in a campaign
exports.findbycampaign = (req, res) => {
    // Get valid token
    const token = Token.getToken(req);
    let Campaign = getCampaignModel(token);
    let Landing = getLandingModel(token);
    // Parameters for the query
    const params = {
        deleted: 0,
        created: 0,
        __v: 0
    };
    // Get the campaign ID sent
    Campaign.findOne({_id: req.params.id}, (err, campaign) => {
        // If use a bad ID
        if (!campaign)
            return Response.getResponse(res, 400, 'None campaign found for this ID', null);
        // Get landings with campaign_ID parameter
        Landing.find({campaign_id: req.params.id}, params, (err, landings) => {
            // If get error, return it
            if (err)
                return Response.getResponse(res, 500, 'Internal server error...', err);
            // Return landings data
            return Response.getResponse(res, 200, 'Landings details fetched...', landings);
        });
    })
};

// Duplicate Landing
exports.duplicate = (req, res) => {
    // Get valid token
    const token = Token.getToken(req);
    // Get Landing Model
    let Landing = getLandingModel(token);
    // Query to get the landing
    const landing_find = Landing.findOne({_id: req.body.id});
    landing_find.findOne((err, find) => {
        // If unautorized user
        if (token.role === 'CUSTOMER')
            return Response.getResponse(res, 401, 'User not authorized', null);
        // If none landing found
        if (!find)
            return Response.getResponse(res, 400, 'None landing found for this ID.', null);
        // Delete the old Landing ID to create new one
        find._id = undefined;
        // Create new landing with the same parameters other landing
        let duplicate_landing = new Landing(JSON.parse(JSON.stringify(find)));
        // Set the new parameters of the duplication
        duplicate_landing.created = new Date();
        duplicate_landing.pageSetting.param.title = req.body.title;
        duplicate_landing.pageSetting.param.permalink = req.body.permalink;
        duplicate_landing.save((err) => {
            // If get error, return it
            if (err)
                return Response.getResponse(res, 500, 'Internal server error...', err);
            // Save the data in database
            return Response.getResponse(res, 200, 'Landings duplicated!', duplicate_landing);
        })
    })
};

