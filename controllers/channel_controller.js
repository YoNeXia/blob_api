/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 09/04/19 09:53
 */

// Import user_model and modules
const User = require('../models/user_model');
const Channel = require('../models/channel_model');

const Token = require('../token_management');
const Mongoose = require('mongoose');

Mongoose.Promise = Promise;


// Create User action
exports.create = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Create new user, with received parameters
    const new_channel = new Channel(req.body);
    // Query to see if have user rights
    const query = User.find({$and: [{_id: token.id}, {role: {$ne: 'CUSTOMER'}}]});
    // Make the action to create new user
    query.findOne((err, user) => {
        // If not authorized user found
        if (!user) {
            return res.status(401).json({
                message: 'User not authorized',
                data: null
            });
        }
        new_channel.save((err) => {
            // You have got an error during save
            if (err)
                return res.status(500).json(err);
            // Save the data in database
            return res.status(200).json({
                message: 'New channel created!',
                data: new_channel
            });
        });
    });
};

// Find all Channels action
exports.all = (req, res) => {
    // Get valid token
    const token = Token.getToken(req);
    // Do the query
    User.find({_id: token.id}, (err, result) => {
        // If have any error
        if (err)
            return res.status(500).json(err);
        // Conversion between MongoDB Object and JS Object (with JSON format)
        result.map((authorization) => {
            // Check authorizations
            switch (authorization.role) {
                // SUPER_ADMIN or ADMIN
                case ('SUPER_ADMIN'):
                case ('ADMIN'):
                    Channel.find({}, (err, channels) => {
                        return res.status(200).json(channels);
                    });
                    break;
                // If user have not any role or not authorized role
                default:
                    res.status(401).json({
                        message: 'User not authorized'
                    });
                    break;
            }
        })
    });
};