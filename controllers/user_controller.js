/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 09/04/19 11:20
 */

// Import modules
const Database = require('../database_management');
const Response = require('../responses');
const Token = require('../token_management');
const Mongoose = require('mongoose');
const Bcrypt = require('bcrypt');

Mongoose.Promise = Promise;

// Return User Model
getUserModel = (token) => {
    return Database.connections['authentication'].users
};

// Return Customer Model
getCustomerModel = (token) => {
    return Database.connections['authentication'].customers
};

// Return User
getUserAuthorization = (req, res, user_model, token) => {
    // Query to see if have user rights
    return user_model.findOne({$and: [{_id: token.id, deleted: false}, {role: {$ne: 'CUSTOMER'}}]})
};

// Create User action
exports.create = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Get User model
    let User = getUserModel(token);
    // Get User authorization
    const query = getUserAuthorization(req, res, User, token);
    query.findOne().then((user_authorization) => {
        // If not authorized user found
        if (!user_authorization)
            return Response.getResponse(res, 401, 'User not authorized', null);
        // Test if email is not used
        User.findOne({email: req.body.email, deleted: false}, (err, duplication) => {
            if (duplication)
                return Response.getResponse(res, 400, 'Email used!', null);
            // Create new user, with received parameters
            const new_user = new User(req.body);
            // Verify if password exist
            if (!req.body.password)
                return Response.getResponse(res, 400, 'Forgot to enter password!', null);
            // Crypt the new password of the new user
            new_user.password = Bcrypt.hashSync(req.body.password, 10);
            // Save the new user
            new_user.save((err) => {
                // If get error, return it
                if (err)
                    return Response.getResponse(res, 500, 'Internal server error...', err);
                // Save the data in database
                return Response.getResponse(res, 200, 'New user created!', new_user);
            });
        });
    })
    // If get error during query to user authorization, return it
        .catch((err) => {
            return Response.getResponse(res, 500, 'Internal server error...', err);
        });
};

// Update by ID User action
exports.update = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Get User model
    let User = getUserModel(token);
    // Get User authorization
    const query = getUserAuthorization(req, res, User, token);
    query.findOne().then((user_authorization) => {
        // If not authorized user found
        if (!user_authorization && req.params.id !== token.id)
            return Response.getResponse(res, 401, 'User not authorized', null);
        // Update the data in database
        User.findOneAndUpdate({_id: req.params.id, deleted: false}, {$set: req.body}, (err, user_update) => {
            // If get error, return it
            if (err)
                return Response.getResponse(res, 500, 'Internal server error...', err);
            // Return User updated
            return Response.getResponse(res, 200, 'User updated!', user_update);
        })
    })
    // If get error during query to user authorization, return it
        .catch((err) => {
            return Response.getResponse(res, 500, 'Internal server error...', err);
        });
};

// Delete by ID User action
exports.delete = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Get User model
    let User = getUserModel(token);
    // Get User authorization
    const query = getUserAuthorization(req, res, User, token);
    query.findOne().then((user_authorization) => {
        // If not authorized user found
        if (!user_authorization && req.params.id !== token.id)
            return Response.getResponse(res, 401, 'User not authorized', null);
        // Update the data in database
        User.findOneAndUpdate({
            _id: req.params.id,
            deleted: false
        }, {$set: {deleted: true}}, (err, user_deleted) => {
            // If get error, return it
            if (err)
                return Response.getResponse(res, 500, 'Internal server error...', err);
            // Return User deleted
            return Response.getResponse(res, 200, 'User deleted!', null);
        })
    })
    // If get error during query to user authorization, return it
        .catch((err) => {
            return Response.getResponse(res, 500, 'Internal server error...', err);
        });
};

// Find all Users action
exports.all = (req, res) => {
    // Parameters for the queries
    const params_super_admin = {
        password: 0,
        location: 0,
        __v: 0
    };
    const params_admin_and_manager = {
        password: 0,
        location: 0,
        __v: 0
    };
    const params_customer = {
        deleted: 0,
        password: 0,
        location: 0,
        role: 0,
        customer_id: 0,
        last_visit: 0,
        __v: 0
    };
    // Get token
    const token = Token.getToken(req);
    // Get User model
    let User = getUserModel(token);
    // Get User authorization
    const query = User.find({$and: [{_id: token.id, deleted: false}]});
    query.findOne().then((user_authorization) => {
        // If not authorized user found
        if (!user_authorization && req.params.id !== token.id)
            return Response.getResponse(res, 401, 'User not authorized', null);
        // Check authorizations
        switch (user_authorization.role) {
            // SUPER_ADMIN
            case ('SUPER_ADMIN'):
                User.find({}, params_super_admin, (err, users) => {
                    // If get error, return it
                    if (err)
                        return Response.getResponse(res, 500, 'Internal server error...', err);
                    // Return Users data
                    return Response.getResponse(res, 200, 'Users fetched...', users);
                }).sort({surname: 1});
                break;
            // ADMIN
            case ('ADMIN'):
            // MANAGER
            case ('MANAGER'):
                User.find({deleted: false}, params_admin_and_manager, (err, users) => {
                    // If get error, return it
                    if (err)
                        return Response.getResponse(res, 500, 'Internal server error...', err);
                    // Return Users data
                    return Response.getResponse(res, 200, 'Users fetched...', users);
                }).sort({surname: 1});
                break;
            // CUSTOMER
            case ('CUSTOMER'):
                User.find({
                    customer_id: token.customer_id,
                    deleted: false
                }, params_customer, (err, users) => {
                    // If get error, return it
                    if (err)
                        return Response.getResponse(res, 500, 'Internal server error...', err);
                    // Return Users data
                    return Response.getResponse(res, 200, 'Users fetched...', users);
                }).sort({surname: 1});
                break;
            // If user have not any role
            default:
                Response.getResponse(res, 401, 'User not authorized', null);
                break;
        }
    })
    // If get error during query to user authorization, return it
        .catch((err) => {
            return Response.getResponse(res, 500, 'Internal server error...', err);
        });
};

// Find by ID User action
exports.find = (req, res) => {
    // Parameters for the query
    const params = {
        deleted: 0,
        password: 0,
        role: 0,
        _id: 0
    };
    // Get token
    const token = Token.getToken(req);
    // Get User model
    let User = getUserModel(token);
    // Get User authorization
    const query = getUserAuthorization(req, res, User, token);
    query.findOne({}).then((user_authorization) => {
        // If not authorized user found
        if (!user_authorization && req.params.id !== token.id)
            return Response.getResponse(res, 401, 'User not authorized', null);
        // Found the user
        User.findOne({_id: req.params.id}, params, (err, find) => {
            // If bad ID used
            if (!find)
                return Response.getResponse(res, 400, 'None user found for this ID.', null);
            // If get error, return it
            if (err)
                return Response.getResponse(res, 500, 'Internal server error...', err);
            // Send the response with user data
            return Response.getResponse(res, 200, 'User details fetched...', find);
        })
    })
    // If get error during query to user authorization, return it
        .catch((err) => {
            return Response.getResponse(res, 500, 'Internal server error...', err);
        });
};

// User login action
exports.login = (req, res) => {
    const token = Token.getToken(req);
    // Get User model
    let User = getUserModel(token);
    // Use variable to return customer_name
    let data;
    // Basic query in all login request
    const query = User.findOne({email: req.body.email, deleted: false}, (err, authorization) => {
        // No data match with email sent
        if (!authorization)
            return Response.getResponse(res, 400, 'User not found! Bad email.', null);
        // Check if encrypt password correspond with password sent
        if (!Bcrypt.compareSync(req.body.password, authorization.password))
            return Response.getResponse(res, 400, 'User not found! Bad password.', null);
        // Update in database the last client visit
        User.updateOne(query, {$set: {last_visit: new Date()}}).then(() => {
        });
        // Make the actions to log client
        query.findOne((err, result) => {
            // If get error, return it
            if (err)
                return Response.getResponse(res, 500, err, null);
            // Parse and stringify result to modify after
            data = JSON.parse(JSON.stringify(result));
            // Get Customer Model
            let Customer = getCustomerModel(token);
            // Find the customer of the user
            Customer.findOne({_id: result.customer_id}, (err, customer) => {
                // Add customer_name to the answer
                if (customer)
                    data.customer_name = customer.name;
                // If not exist, add null name
                else
                    data.customer_name = null;
                // Any token error, recreate it or not existing token
                const new_token = Token.createToken(result.email, result.id, data.customer_name, result.role);
                new_token.then((value) => {
                    // Add token to the answer
                    data.token = value;
                    // Return User data
                    return Response.getResponse(res, 200, 'User details fetched...', data)
                })
            })
        });
    });
};

// User authorized modify token action
exports.modifyTokenCustomer = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Get User model
    let User = getUserModel(token);
    // Get User authorization
    const query = getUserAuthorization(req, res, User, token);
    query.findOne({}).then((user_authorization) => {
        // If not authorized user found
        if (!user_authorization)
            return Response.getResponse(res, 401, 'User not authorized', null);
        // If user not send customer_name
        if (!req.body.customer_name)
            return Response.getResponse(res, 400, 'Forgot customer_name parameter...');
        // Create a new token for an authorized user
        const new_token = Token.createToken(user_authorization.email, user_authorization.id, req.body.customer_name, user_authorization.role);
        new_token.then((value) => {
            // Return Token data
            return Response.getResponse(res, 200, 'Admin token for customer ' + req.body.customer_name + ' fetched...', value)
        })
    })    // If get error during query to user authorization, return it
        .catch((err) => {
            return Response.getResponse(res, 500, 'Internal server error...', err);
        });
};