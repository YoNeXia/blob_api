/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 09/04/19 10:24
 */

// Import modules
const Token = require('../token_management');
const Database = require('../database_management');
const Response = require('../responses');
const Mongoose = require('mongoose');
const ChildProcess = require('child-process-promise');
const FileSystem = require('fs-extra');

Mongoose.Promise = Promise;

// Return Customer Model
getCustomerModel = (token) => {
    return Database.connections['authentication'].customers
};

// Return User Model
getUserModel = (token) => {
    return Database.connections['authentication'].users
};

// Return User
getUserAuthorization = (req, res, user_model, token) => {
    // Query to see if have user rights
    return user_model.findOne({$and: [{_id: token.id, deleted: false}, {role: {$ne: 'CUSTOMER'}}]});
};

// Create Customer action
exports.create = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Get User model
    let User = getUserModel(token);
    // Get Customer model
    let Customer = getCustomerModel(token);
    // Get User authorization
    const query = getUserAuthorization(req, res, User, token);
    query.findOne().then((user_authorization) => {
        // If not authorized user found
        if (!user_authorization)
            return Response.getResponse(res, 401, 'User not authorized', null);
        // Create new customer, with received parameters
        const new_customer = new Customer(req.body);
        // Execute a script to create the new database
        ChildProcess.exec('bash create_db_script.sh ' + new_customer.name + ' ' + new_customer._id, (err, stdout, stderr) => {
            // If get error, return it
            if (err)
                return Response.getResponse(res, 400, 'Customer exist... Or any parameter missing...', err);
            // Save the new customer in database
            new_customer.save((err) => {
                // If get error, return it
                if (err)
                    return Response.getResponse(res, 500, 'Internal server error...', err);
                // Save the data in database
                return Response.getResponse(res, 200, 'New customer created!', new_customer);
            });
        });
    }).catch((err) => {
        return Response.getResponse(res, 500, 'Internal server error...', err);
    })
};

// Update by ID Customer action
exports.update = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Get User model
    let User = getUserModel(token);
    // Get Customer model
    let Customer = getCustomerModel(token);
    // Get User authorization
    const query = getUserAuthorization(req, res, User, token);
    query.findOne().then((user_authorization) => {
        // If not authorized user found
        if (!user_authorization)
            return Response.getResponse(res, 401, 'User not authorized', null);
        // If user try to change customer_name
        if (req.body.name)
            return Response.getResponse(res, 400, 'Can\'t change customer name!', null);
        // Update the data in database
        Customer.findOneAndUpdate({
            _id: req.params.id,
            deleted: false
        }, {$set: req.body}, (err, customer_update) => {
            // If get error, return it
            if (err)
                return Response.getResponse(res, 500, 'Internal server error...', err);
            // If no ID for customer is found
            if (!customer_update)
                return Response.getResponse(res, 400, 'No customer found for this ID.', err);
            // Return Customer updated
            return Response.getResponse(res, 200, 'Customer updated!', customer_update);
        })
    })
    // If get error during query to user authorization, return it
        .catch((err) => {
            return Response.getResponse(res, 500, 'Internal server error...', err);
        })
};

// Delete by ID Customer action
exports.delete = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Get User model
    let User = getUserModel(token);
    // Get Customer model
    let Customer = getCustomerModel(token);
    // Get User authorization
    const query = getUserAuthorization(req, res, User, token);
    query.findOne().then((user_authorization) => {
        // If not authorized user found
        if (!user_authorization)
            return Response.getResponse(res, 401, 'User not authorized', null);
        // Update the data in database
        Customer.findOneAndUpdate({
            _id: req.params.id,
            deleted: false
        }, {$set: {deleted: true}}, (err) => {
            // If get error, return it
            if (err)
                return Response.getResponse(res, 500, 'Internal server error...', err);
            // Return Customer updated
            return Response.getResponse(res, 200, 'Customer deleted!', null);
        })
    })
    // If get error during query to user authorization, return it
        .catch((err) => {
            return Response.getResponse(res, 500, 'Internal server error...', err);
        })
};

// Find all Customers action
exports.all = (req, res) => {
    // Parameters for the queries
    const params_super_admin = {
        location: 0,
        campaign_id: 0,
        __v: 0,
    };
    const params_admin_and_manager = {
        deleted: 0,
        location: 0,
        campaign_id: 0,
        __v: 0,
    };
    // Get token
    const token = Token.getToken(req);
    // Get User model
    let User = getUserModel(token);
    // Get Customer model
    let Customer = getCustomerModel(token);
    // Get User authorization
    const query = getUserAuthorization(req, res, User, token);
    query.findOne().then((user_authorization) => {
        // If not authorized user found
        if (!user_authorization)
            return Response.getResponse(res, 401, 'User not authorized', null);
        // Check authorizations
        switch (user_authorization.role) {
            // SUPER_ADMIN
            case ('SUPER_ADMIN'):
                Customer.find({}, params_super_admin, (err, customers) => {
                    // If get error, return it
                    if (err)
                        return Response.getResponse(res, 500, 'Internal server error...', err);
                    // Return Customers data
                    Response.getResponse(res, 200, 'Customers fetched...', customers);
                });
                break;
            // ADMIN
            case('ADMIN'):
            // MANAGER
            case ("MANAGER"):
                Customer.find({deleted: false}, params_admin_and_manager, (err, customers) => {
                    // If get error, return it
                    if (err)
                        return Response.getResponse(res, 500, 'Internal server error...', err);
                    // Return Customers data
                    Response.getResponse(res, 200, 'Customers fetched...', customers);
                });
                break;
            // If user have not any role
            default:
                Response.getResponse(res, 401, 'User not authorized', null);
                break;
        }
    })
    // If get error during query to user authorization, return it
        .catch((err) => {
            return Response.getResponse(res, 500, 'Internal server error...', err);
        });
};

// Find by ID Customer action
exports.find = (req, res) => {
    // Parameters for the query
    const params = {
        deleted: 0,
        campaign_id: 0,
        __v: 0,
        _id: 0
    };
    // Get token
    const token = Token.getToken(req);
    // Get User model
    let User = getUserModel(token);
    // Get Customer model
    let Customer = getCustomerModel(token);
    // Get User authorization
    const query = getUserAuthorization(req, res, User, token);
    query.findOne().then((user_authorization) => {
        // Found the customer
        Customer.findOne({_id: req.params.id, deleted: false}, params, (err, find) => {
            // If not authorized user found
            if (!user_authorization) {
                // Check if the user want to see his customer
                if (req.params.id === token.customer_id) {
                    // Send the response with customer data
                    return Response.getResponse(res, 200, 'Customer details fetched...', find);
                }
                // Send Unauthorized
                return Response.getResponse(res, 401, 'User not authorized', null);
            }
            // If bad ID used
            if (!find)
                return Response.getResponse(res, 400, 'None customer found for this ID.', null);
            // If get error, return it
            if (err)
                return Response.getResponse(res, 500, 'Internal server error...', err);
            // Send the response with customer data
            return Response.getResponse(res, 200, 'Customer details fetched...', find);
        })
    })
    // If get error during query to user authorization, return it
        .catch((err) => {
            return Response.getResponse(res, 500, 'Internal server error...', err);
        });
};