/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 09/04/19 11:36
 */

// Import modules
const FileSystem = require('fs-extra');
const Response = require('../responses');


// Get version in production
exports.version = (req, res) => {
    // Read package.json and convert into string
    const file = FileSystem.readFileSync('package.json').toString();
    // Convert file variable to object JSON
    const package_json = JSON.parse(file);
    // Return answer
    return Response.getResponse(res, 200, 'Version of API fetched...', package_json.version);
};