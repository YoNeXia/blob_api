/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 09/04/19 14:12
 */

// Import modules
const Database = require('../database_management');
const Response = require('../responses');
const Token = require('../token_management');
const Mongoose = require('mongoose');

Mongoose.Promise = Promise;

// Return Campaign Model
getCampaignModel = (token) => {
    return Database.connections[Database.getCustomer(token)].campaigns
};

// Create Campaign action
exports.create = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Get Campaign Model
    let Campaign = getCampaignModel(token);
    // Create new campaign, with received parameters
    const new_campaign = new Campaign(req.body);
    // Set the date when campaign was created
    new_campaign.created = new Date();
    // Save your new campaign
    new_campaign.save((err) => {
        // If get error, return it
        if (err)
            return Response.getResponse(res, 500, 'Internal server error...', err);
        // Save the data in database
        return Response.getResponse(res, 200, 'New campaign created!', new_campaign);
    });
};

// Update by ID Campaign action
exports.update = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Get Campaign Model
    let Campaign = getCampaignModel(token);
    // Find campaign you want to change data
    const campaign_update = Campaign.findOne({_id: req.params.id, deleted: false}, (err, find) => {
        // If use a bad ID
        if (!find)
            return Response.getResponse(res, 400, 'None campaign found for this ID.', null);
    });
    // Update the data in database
    campaign_update.updateOne({$set: req.body}, (err) => {
        // If get error, return it
        if (err)
            return Response.getResponse(res, 500, 'Internal server error...', err);
        // Return Campaign updated
        return Response.getResponse(res, 200, 'Campaign updated!', null);
    })
};

// Delete by ID Campaign action
exports.delete = (req, res) => {
    // Get token
    const token = Token.getToken(req);
    // Get Campaign Model
    let Campaign = getCampaignModel(token);
    // Find campaign you want to delete data
    const campaign_deleted = Campaign.findOne({_id: req.params.id, deleted: false}, (err, find) => {
        if (token.role === 'CUSTOMER')
            return Response.getResponse(res, 401, 'User not authorized', null);
        // If use a bad ID
        if (!find)
            return Response.getResponse(res, 400, 'None campaign found for this ID.', null);
    });
    // Delete the data in database
    campaign_deleted.updateOne({$set: {deleted: true}}, (err) => {
        // If get error, return it
        if (err)
            return Response.getResponse(res, 500, 'Internal server error...', err);
        // Return Campaign deleted
        return Response.getResponse(res, 200, 'Campaign deleted!', null);
    })
};

// Find all Campaigns action
exports.all = (req, res) => {
    // Get valid token
    const token = Token.getToken(req);
    // Get Campaign Model
    let Campaign = getCampaignModel(token);
    // Parameters for the queries
    const params_super_admin = {
        landing_id: 0,
        __v: 0
    };
    const params_admin_and_manager_and_customer = {
        deleted: 0,
        landing_id: 0,
        __v: 0
    };
    // Check authorizations
    switch (token.role) {
        // SUPER_ADMIN
        case ('SUPER_ADMIN'):
            Campaign.find({}, params_super_admin, (err, campaigns) => {
                // If get error, return it
                if (err)
                    return Response.getResponse(res, 500, 'Internal server error...', err);
                // Return campaigns data
                return Response.getResponse(res, 200, 'Campaigns details fetched...', campaigns);
            }).sort({created: -1});
            break;
        // ADMIN or MANAGER or CUSTOMER
        case ('ADMIN'):
        case ('MANAGER'):
        case ('CUSTOMER'):
            Campaign.find({deleted: false}, params_admin_and_manager_and_customer, (err, campaigns) => {
                // If get error, return it
                if (err)
                    return Response.getResponse(res, 500, 'Internal server error...', err);
                // Return campaigns data
                return Response.getResponse(res, 200, 'Campaigns details fetched...', campaigns);
            }).sort({created: -1});
            break;
        // If user have not authorization
        default:
            Response.getResponse(res, 401, 'User not authorized', null);
            break;
    }
};

// Find by ID Campaign action
exports.find = (req, res) => {
    // Get valid token
    const token = Token.getToken(req);
    // Get Campaign Model
    let Campaign = getCampaignModel(token);
    // Parameters for the query
    const params = {
        deleted: 0,
        created: 0,
        __v: 0
    };
    // Find campaign you want to find data
    const campaign_find = Campaign.find({_id: req.params.id, deleted: false});
    // Found the actual campaign
    campaign_find.findOne({}, params, (err, find) => {
        // If request not answer any campaign
        if (!find)
            return Response.getResponse(res, 400, 'None campaign found for this ID.', null);
        // If get error, return it
        if (err)
            return Response.getResponse(res, 500, 'Internal server error...', err);
        // Send the response with campaign data
        return Response.getResponse(res, 200, 'Campaigns details fetched...', find);
    })
};