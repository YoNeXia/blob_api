/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 09/04/19 15:42
 */

// Set and format all responses
getResponse = (res, status, message, data) => {
    // If is not a status number sent
    if (typeof status !== 'number')
        return res.status(500).json({
            message: 'Internal code status error...'
        });
    // Return a response with the parameters sent
    return res.status(status).json({
        message: message,
        data: data,
    })
};
// Export function to be use in all files
module.exports.getResponse = getResponse;