# File docker-compose.yml

#### How docker-compose.yml file set up docker container.

-----------------
Find the **[documentation](https://docs.docker.com/compose/compose-file/)** about docker-compose.yml files.

-----------------
We use a docker-compose.yml file to simplify the installation of the docker container on a server. Indeed, all informations needed to build the container are in one location. **BE CAREFUL with the indention of this file.**

First, we have the version of the docker-compose used.

    # Version of docker-compose using for this file
    version: "3.7"
    
After, we found the services. They include all configurations for the container. You can create multiple services and first parameter is his name.

     # Set the different configurations for your container(s)
     services:
     #  Give a service name
       blob_api:
       
Now we set the future docker container.

         # Select the image you need to install in your container
             image: node:11
         # Give a container name
             container_name: "blob_api"
         # Set up your volumes
             volumes:
               - type: volume
                 source: vol_blob_api
                 target: /usr/src/blob_api
               - type: bind
                 source: /etc/certificates/
                 target: /etc/certificates/
         # Set the working directory in your container
             working_dir: /usr/src/blob_api
         # Set all ports can be use by the container
             ports:
               - 8080:8080
               - 8081:8081
         # Set the command you are launch when container is running
             command: "npm start"
     
     
 And finally, we give a name at volumes with a type _"volume"_. It is more easy to found and backup your volume after.
 
      # Configure your volumes
      volumes:
        vol_blob_api:
          name: vol_blob_api
