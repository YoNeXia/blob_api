
# File package.json  

#### Description about the different key in package.json.  

-----------------  
**package.json** is a central repository of configuration for tools you need into your Node project.  

-----------------  
Descriptions about keys in this package.json.  

**author :** This key permit to give all informations about the author of the program.  

	  "author": {
        "name": "SEPPA",
        "email": "sysadmin@agence-seppa.com",
        "url": "https://agence-seppa.com"
      },  
	
**maintainers :** If any body work on this project, he can put his informations here.  

      "maintainers": [
        {
          "name": "YoNeXia",
          "email": "yohann.neraud@viacesi.fr",
          "url": "https://gitlab.com/YoNeXia"
        }
      ],  

**description :** Brief description of the application.  

	"description": "Blob's Node.js API",  

**name :** The name of the application.  

	"name": "blob_api",  

**version :** The actual application version used.  

    "version": "0.0.1-DEV",  

**main :** Set the entry point of the application. _In this application, it is the app.js file._  

	"main": "app.js",  

**homepage, readme, repository :** All url where you can find the project or informations about it.  

    "homepage": "https://git.seppa.io/blob/api",
    "readme": "https://git.seppa.io/blob/api",
    "repository": {
        "type": "git",
        "url": "https://git.seppa.io/blob/api"
    },  

**keywords :** Keywords associates with what the application does.  

	"keywords": [ "blob", "API", "node.js", "mongo" ],  

**dependencies, devDependencies :** Set a lists of npm packages installed as dependencies or development dependencies. _Find all informations about a package on [NPM website](https://www.npmjs.com/)._  

	"dependencies": { 
		"bcrypt": "^3.0.4", 
		"body-parser": "^1.18.3", 
		"cookie-session": "^1.3.3", 
		"ejs": "^2.6.1", 
		"express": "^4.16.4", 
		"fs-extra": "^7.0.1", 
		"http": "^0.0.0", 
		"https": "^1.0.0", 
		"jsonwebtoken": "^8.5.0", 
		"mongodb": "^3.1.13", 
		"mongoose": "^5.4.17", 
		"mongoose-int32": "^0.3.1" 
	}, 
	"devDependencies": { 
		"nodemon": "^1.18.10" 
	},  

**scripts :** Define a set of node scripts you can run.  

	"scripts": { 
		"start": "./blob_api_script.sh", 
		"test": "" 
	}