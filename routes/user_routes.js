/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 08/04/19 14:50
 */

// Import express router module
const Route = require('express').Router();
// Import user_controller
const UserController = require('../controllers/user_controller');

// Set user login route : /users/login
Route.route('/login')
    .post(UserController.login);

// Set user admin route : /users/admin
Route.route('/modify_token/customer')
    .post(UserController.modifyTokenCustomer);

// Set user CRUD routes : /users
Route.route('/')
    .put(UserController.create);
Route.route('/:id')
    .post(UserController.update);
Route.route('/:id')
    .delete(UserController.delete);
Route.route('/')
    .get(UserController.all);
Route.route('/:id')
    .get(UserController.find);

// Exports user routes
module.exports = Route;