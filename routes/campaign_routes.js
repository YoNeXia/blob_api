/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 08/04/19 15:03
 */

// Import express router module
const Route = require('express').Router();
// Import campaign_controller
const CampaignController = require('../controllers/campaign_controller');

// Set campaign CRUD routes : /campaign/..
Route.route('/')
    .put(CampaignController.create);
Route.route('/:id')
    .post(CampaignController.update);
Route.route('/:id')
    .delete(CampaignController.delete);
Route.route('/')
    .get(CampaignController.all);
Route.route('/:id')
    .get(CampaignController.find);

// Exports campaign routes
module.exports = Route;