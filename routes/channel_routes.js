/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 01/04/19 12:51
 */

// Import express router module
const Route = require('express').Router();
// Import channel_controller
const ChannelController = require('../controllers/channel_controller');

// Set channel CRUD route : /channel/...
Route.route('/create')
    .post(ChannelController.create);
Route.route('/all')
    .get(ChannelController.all);

// Exports channel routes
module.exports = Route;