/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 01/04/19 12:51
 */

// Import express router module
const Route = require('express').Router();
// Import customer_controller
const CustomerController = require('../controllers/customer_controller');

// Set customer CRUD routes : /customers
Route.route('/')
    .put(CustomerController.create);
Route.route('/:id')
    .post(CustomerController.update);
Route.route('/:id')
    .delete(CustomerController.delete);
Route.route('/')
    .get(CustomerController.all);
Route.route('/:id')
    .get(CustomerController.find);

// Exports customer routes
module.exports = Route;