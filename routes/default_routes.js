/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 01/04/19 12:51
 */

// Import express router module
const Route = require('express').Router();
// Import default_controller
const DefaultController = require('../controllers/default_controller');

// Set default routes : /...
Route.route('/version')
    .get(DefaultController.version);

// Exports default routes
module.exports = Route;