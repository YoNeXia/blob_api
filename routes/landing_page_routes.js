/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 01/04/19 12:51
 */

// Import express router module
const Route = require('express').Router();
// Import user_controller
const LandingController = require('../controllers/landing_page_controller');

// Set user CRUD routes : /landings
Route.route('/')
    .put(LandingController.create);
Route.route('/duplicate')
    .put(LandingController.duplicate);
Route.route('/:id')
    .post(LandingController.update);
Route.route('/:id')
    .delete(LandingController.delete);
Route.route('/:id')
    .get(LandingController.find);
Route.route('/campaign/:id')
    .get(LandingController.findbycampaign);


// Exports user routes
module.exports = Route;