/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 09/04/19 15:34
 */

// Import JWT and modules
const JWT = require('jsonwebtoken');
const FileSystem = require('fs-extra');
const Response = require('./responses');

// Import certificates files
const private_key = "private_pass";//FileSystem.readFileSync('/etc/certificates/ca.key.pem');
const public_key = "public_pass";//FileSystem.readFileSync('/etc/certificates/ca.cert.pem');

// Generate new token for an user
exports.createToken = (email, id, customer_name, role) => {
    // JWT method to generate token with data schema
    return new Promise((resolve, reject) => {
        return JWT.sign({
                // Data schema in token
                id: id,
                email: email,
                customer_name: customer_name,
                role: role
            }, // Token key to crypt
            private_key,
            // Token expiration time (in seconds)
            {
                //algorithm: 'RS256',
                algorithm: 'HS256',
                expiresIn: 6 * 60 * 60
            },
            // Token creation
            (err, token) => {
                resolve(token);
                reject(err);
            })
    });
};

// Check actual client token
decryptToken = (token) => {
    // JWT method to check an existing token
    return JWT.verify(
        // Current token form web navigator
        token,
        // Token key to decrypt
        public_key,
        (err, decoded) => {
            // Token creation
            if (err) {
                return 'TokenError';
            }
            // Valid token
            else {
                return decoded;
            }
        });
};

// Permit to redirect User to login page if necessary
backToLogin = (res, token) => {
    // Check token state
    switch (token) {
        case 'TokenError':
            return true;
        default:
            return token;
    }
};

exports.checkToken = (req, res, next) => {
    if (req.url.includes('/users/login') || req.url.includes('/version')) {
        next();
    } else {
        // Get current token and check it
        const token = getToken(req, res);
        // Back to login page if token are bad
        if (backToLogin(res, token) === true)
            return Response.getResponse(res, 498, 'Invalid token, back to login.', token);
        next();
    }
};

// Get actual client token
getToken = (req) => {
    // Get Bearer <token> from the client
    let token = req.headers['authorization'];
    // Postman token
    /*let bearerHeader = req.headers['authorization'];
    let bearer;
    let token;
    if (bearerHeader) {
        bearer = bearerHeader.split(' ');
        token = bearer[1];
    }*/
    // Split the Bearer <token> to keep only the token
    if (typeof token !== 'undefined')
        return decryptToken(token);
    // If none token exist
    else
        return decryptToken('');
};
exports.getToken = getToken;