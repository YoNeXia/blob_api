/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 09/04/19 15:17
 */

// Import modules
const Express = require('express');
const Body_parser = require('body-parser');
const Http = require('http');
const CORS = require('cors');
// Get routes
const UserRoutes = require('./routes/user_routes');
const CustomerRoutes = require('./routes/customer_routes');
const CampaignRoutes = require('./routes/campaign_routes');
const LandingRoutes = require('./routes/landing_page_routes');
const ChannelRoutes = require('./routes/channel_routes');
const DefaultRoutes = require('./routes/default_routes');
// Get Token
const Token = require('./token_management');
// Get database connections
const Database = require('./database_management');

// Initialize the application
const App = Express();

// Set the control access
App.use(CORS());

// Configure body-parser to handle post requests
App.use(Body_parser.urlencoded({
    extended: true
}));
App.use(Body_parser.json());
// Check token for any user
App.use(Token.checkToken);
// Connect to the DataBase
App.use(Database.databaseConnections);

// Use default routes
App.use('/', DefaultRoutes);
// Use user routes
App.use('/users', UserRoutes);
// Use customer routes
App.use('/customers', CustomerRoutes);
// Use campaign routes
App.use('/campaigns', CampaignRoutes);
// Use landing page routes
App.use('/landings', LandingRoutes);
// Use channel routes
//App.use('/channels', ChannelRoutes);

// Launch the application and set the port to 8080
Http.createServer(App).listen(8080, (err) => {
    if (!err)
        console.log("API ready and HTTP listen on port 8080.");
    else
        console.log(err)
});