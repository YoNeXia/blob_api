#!/bin/bash
# This script create a new database in blob_database container when new customer is created in authentication database.
# It need jq.
# How execute: ./create_db_script.sh <customer_name>

tmp=$(mktemp)

server_user="user"
server_pass="password"
server_url="url_server.com"

volume_path="/var/lib/docker/volumes/blob_resources/_data/"${1}"/"${2}"/"
directories=('images/' 'videos/' 'favicons/' 'fonts/')

if [[ $1 && $2 ]]
then
echo "Creation of the new database with secure user..."
sshpass -p ${server_pass} ssh -p 53994 ${server_user}@${server_url} 'cat /home/sysadmin/privatePwd | sudo -S docker exec blob_database usr/bin/mongo '${1}' -u blob_root -p password --authenticationDatabase admin --eval "db.createUser({ user: \"blob_api_user\", pwd: \"password\", roles: [{role: \"readWrite\", db: \"'${1}'\"}]})"'

echo "Creation of all customer files needed to stock files..."
sshpass -p ${server_pass} ssh -p 53994 ${server_user}@${server_url} 'cat /home/sysadmin/privatePwd | sudo -S mkdir /var/lib/docker/volumes/blob_resources/_data/'${1}'/'
sshpass -p ${server_pass} ssh -p 53994 ${server_user}@${server_url} 'cat /home/sysadmin/privatePwd | sudo -S mkdir '${volume_path}
sshpass -p ${server_pass} ssh -p 53994 ${server_user}@${server_url} 'cat /home/sysadmin/privatePwd | sudo -S touch '${volume_path}'index.php'

for item in ${directories[*]}
do
echo 'toto'
    sshpass -p ${server_pass} ssh -p 53994 ${server_user}@${server_url} 'cat /home/sysadmin/privatePwd | sudo -S mkdir '${volume_path}${item}
    sshpass -p ${server_pass} ssh -p 53994 ${server_user}@${server_url} 'cat /home/sysadmin/privatePwd | sudo -S touch '${volume_path}${item}'index.php'
done
echo 'Add new configuration DB in config.json...'
jq '.databases += [{name : "'${1}'", "host": "url_db_server.com", "port": "27017"}]' config.json  > ${tmp} && mv ${tmp} config.json
else
    echo "Name customer is empty!"
    exit 1
fi