/*
 * Copyright SEPPA
 * Author: YoNeXia <yohann.neraud@viacesi.fr> (https://gitlab.com/YoNeXia)
 * Git: https://git.seppa.io/blob/blobAPI
 *
 * Date: 09/04/19 15:40
 */

// Import modules
const FileSystem = require('fs-extra');
const Mongoose = require('mongoose');
const Token = require('./token_management');
// Import models
const User = require('./models/user_model');
const Customer = require('./models/customer_model');
const Campaign = require('./models/campaign_model');
const Landing = require('./models/landing_page_model');

// Create empty connections
const connections = [];

// Get configuration of databases
let configDatabase = FileSystem.readFileSync('config.json');
configDatabase = JSON.parse(configDatabase);

// Add databases connections
exports.databaseConnections = (req, res, next) => {
    // Get database name
    let database_name = getCustomer(Token.getToken(req));
    // Route automatically connect in database 'authentication'
    if (req.url.includes('/users') || req.url.includes('/customers'))
        database_name = 'authentication';
    // If connection exist
    if (connections[database_name]) {
        next();
    }
    // Create connection
    else {
        // Add a new connection into tab connections
        for (let i = 0; i < configDatabase.databases.length; i++) {
            if (configDatabase.databases[i].name === database_name) {
                connections[database_name] = {};
                // Set the connection url with the file config.json
                let url = 'mongodb://' + configDatabase.user + ':' + configDatabase.pass + '@' + configDatabase.databases[i].host + ':' + configDatabase.databases[i].port + '/' + configDatabase.databases[i].name + '?ssl=true';
                // Connect the database
                connections[database_name].db = Mongoose.createConnection(url, {useNewUrlParser: true});
                // Uploads schemas
                if (database_name === 'authentication') {
                    // User schema
                    connections[database_name].users = connections[database_name].db.model('users', User.user_schema);
                    // Customer schema
                    connections[database_name].customers = connections[database_name].db.model('customers', Customer.customer_schema);
                } else {
                    // Campaign schema
                    connections[database_name].campaigns = connections[database_name].db.model('campaigns', Campaign.campaign_schema);
                    // Landing schema
                    connections[database_name].landings = connections[database_name].db.model('landings', Landing.landing_schema);
                }
            }
        }
        // Continue
        next();
    }
};

// Get customer name saved in token
getCustomer = (token) => {
    // If no customer_name
    if (!token.customer_name)
        return 'authentication';
    // Return customer_name
    return token.customer_name;
};
// Export function to be use in all files
exports.getCustomer = getCustomer;

// Exports databases connections
module.exports.connections = connections;